// ****************************************************************
// Kollmorgen AKD(c) Drive interface with motion profile calculation on the drive (DS402 Mode of Operation 1)
//
// DISCLAIMER:
// All programs in this release are provided "AS IS, WHERE IS", WITHOUT ANY WARRENTIES, EXPRESS OR IMPLIED.
// There may be technical or editorial omissions in the programs and their specifications.
// These programs are provided solely dor user appliucation development and user assumes all responsibility for their use.
// Programsand their content are subject to change without notice
// 
// Version	Date		Who			What
// ****************************************************************
// V 1.000	25.09.20	MRupf Oxni	First version
// ****************************************************************
#pragma once

//*** Required header files ***
#include <windows.h>
#include <stdint.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\Sha64Debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"

//*** Structures need to have 1 byte alignment ***
#pragma pack(push, old_alignment)
#pragma pack(1)

//*** TX mapping structure [Device Output] ***
struct TX_MAP_AKD_v1 {
	uint16_t statusWord;	// h1a00 2Byte	status word
	int16_t actCurrent;	// h1a00 2Byte	Actual current
	uint32_t digitalInput;	// h1a00 4Byte	Digital input
	int32_t actPos;			// h1a01 4Byte	Actual Position
	int32_t actVel;			// h1a01 4Byte	Actual velocity
	uint8_t  dummy;			// h1a02 1Byte	Filler
	uint8_t  opMode;		// h1a02 1Byte	Actual mode of operation
};

//*** RX mapping structure [Device Input]  ***
struct RX_MAP_AKD_v1 {
	uint8_t dummy;			// h1600 1Byte	filler
	uint16_t controlWord;	// h1600 2Byte	contorl word
	uint8_t opMode;			// h1600 1Byte	mode of operation
	uint32_t digitalOutput;	// h1600 4Byte	digital output
	int32_t cmdPos;			// h1601 4Byte	target position
	uint32_t cmdVel;		// h1601 4Byte	target velocity
	uint32_t cmdAcc;		// h1602 4Byte	target acceleration
	uint32_t cmdDec;		// h1602 4Byte	target deceleration
};

//Set back old alignment
#pragma pack(pop, old_alignment)

// AKD Class
class AkdMotionTask {
private:
	bool _enable = false;		// Enable/Disable Drive
	bool _isEnabled = false;
	bool _doReset = false;		// Enable routine will include reset in next call
	bool _doHome = false;		// Enable routine will control homing procdure
	bool _homeDone = false;		// Mirror of Home Done bit in Opmode Home
	bool _error = false;			// If any action fails
	bool _moveRel = false;		// Immediatly start relative move
	bool _moveAbs = false;		// Immediatly start absolute move
	bool _moveJog = false;		// Start jog at constant velocity
	bool _stop = false;			// Dont' allow any motion
	int32_t _cmdPos = 0;
	int32_t _cmdVel = 0;
	int32_t _cmdAcc = 0;
	int32_t _cmdDec = 0;
	uint8_t _actMode = 0;
	double _posScaling;				// User Units per revolution
	float _driveIcont;				// Nominal current of the drive to scale actual current
	uint32_t _digitalOutput = 0;			// output are set zero by default
	int64_t _internalPos = 0;				// Internal position upscaled to 64 bit from 32 bit EtherCat
	int64_t _internalPosOverflow = 0;			// INternal position overflow counter
	uint32_t _counter = 0;
	const TX_MAP_AKD_v1* txMap;
	RX_MAP_AKD_v1* rxMap;

public:
	// SW Version
	static const uint32_t SW_VERSION = 0x1000;

	// Constructor
	AkdMotionTask(double iPosScaling, float iDriveCurr);

	// Return drive fault
	bool getDriveFault();

	// Return drive warning
	bool getDriveWarning();

	// Return home status
	bool getHomeStatus();

	// Return error status
	bool getErrorStatus();

	// Return enable status
	bool getEnableStatus();

	// Return drive stop bit
	bool getDriveStop();

	// Return status as bit combined int
	uint32_t getDriveStatus();

	// Get Actual Position as float in User Units
	double getActPosUU();

	// Get Actual Speed in RPM
	double getActVel();

	// Get Actual Current in Arms
	double getActCurrent();

	// Get digital input and output status
	uint32_t getDigInStat();


	// REALTIME TASK this routine has to be called in each cycle
	// Mapping of all commands to and from the EtherCat
	void realTimeTask(const TX_MAP_AKD_v1* txMap, RX_MAP_AKD_v1* rxMap);
	

	// Enable/Disable procedure
	void enableDrive(bool);

	// Stop any motion in progress
	void setStop(bool);
	
	// Reset drive faults
	void resetFault();
	
	// Home axis
	void homeAxis();
	
	// Move Relative inputs scaled in UU
	void moveRelUU(double moveDistance, double moveVel, double moveAcc, double moveDec);
	
	// Move Absolute inputs scaled in UU
	void moveAbsUU(double targetPos, double moveVel, double moveAcc, double moveDec);
	
	// Jog inputs scaled in UU
	void moveVel(double moveVel, double moveAcc, double moveDec);
	
	// Set digital output  (outIdx 1 or 2 are only valid)
	void setDigOut(int outIdx, bool value);

	// Set Move data in ECAT bus units
	void moveDataInc(LONG iMoveDist, LONG iMoveVel, LONG iMoveAcc, LONG iMoveDec);

	
	// SDO control to the drive
	static uint64_t sendSDOCmd(PSTATION_INFO pStation, bool iWrite, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data);

	// SDO download request
	static uint64_t sdoControl_AKD(PSTATION_INFO);

};

