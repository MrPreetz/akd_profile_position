// *****************************************************************
// DEFINES - Pool for all global constant 
// 
// DISCLAIMER:
// All programs in this release are provided "AS IS, WHERE IS", WITHOUT ANY WARRENTIES, EXPRESS OR IMPLIED.
// There may be technical or editorial omissions in the programs and their specifications.
// These programs are provided solely dor user appliucation development and user assumes all responsibility for their use.
// Programsand their content are subject to change without notice
// 
// Version	Date		Who			What
// *****************************************************************
// V 1.000	25.09.20	MRupf Oxni	First version
// *****************************************************************
#pragma once
#include <stdint.h>

// SW Version
static const uint32_t SW_VERSION_DEFINES = 0x1000;

// Constants Real-Time
const uint32_t REALTIME_PERIOD = 2000;	// Realtime sampling period [usec]
const uint32_t SYNC_CYCLES = 1;			// EtherCat Cycle time 2ms = 2 * 1000usec
const uint32_t CYCLE_TIME_IN_MS = 0x00000002;

// Constants AKD
const double POS_SCALE_DRIVE_AKD = 1048576;		// 2^FB1.PSCALE

const uint16_t BIT_MASK_DRIVE_FAULT_AKD = 0x0008;	// Bit 3
const uint16_t BIT_MASK_DRIVE_WARNING_AKD = 0x0080;	// Bit 7
const uint16_t BIT_MASK_DRIVE_STO_AKD = 0x0100;		// Bit 8
const uint16_t BIT_MASK_DRIVE_STATUS_AKD = 0x007F;	// Bit 0-6
const uint16_t BIT_MASK_DRIVE_CMD_AKD = 0x000F;		// Bit 0-3
const uint16_t BIT_MASK_DRIVE_RESET_AKD = 0x0080;	// Bit 7
const uint16_t BIT_MASK_DRIVE_STOP_AKD = 0x0100;	// Bit 8

// Profile Position Mode, Home Mode
const uint16_t BIT_MASK_DRIVE_MAN1_AKD = 0x1000;	// Bit 12	setpoint acknowledged,	Home done
const uint16_t BIT_MASK_DRIVE_MAN2_AKD = 0x2000;	// Bit 13	following error,		Home error
const uint32_t JOG_TIMEOUT_COUNT = 0;				// Number of realtime cycles till jog request will stop itself, to disable set 0

// Visualization 
const uint32_t MAX_NO_CHANNELS = 256;	// Maximum number of channels to be used will be 1 to MAX_NO_CHANNELS
const uint32_t MAX_NO_DRIVE = 1;		// Maximum number of drive commands set by server