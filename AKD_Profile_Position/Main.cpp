//***************************************
//  Automated generated code template ***
//  EtherCAT Code Type : [Expert DC]  ***
//***************************************

// *****************************************************************
// Main Routine, splitted into Realtime- and Non- Realtime tasks
// 
// DISCLAIMER:
// All programs in this release are provided "AS IS, WHERE IS", WITHOUT ANY WARRENTIES, EXPRESS OR IMPLIED.
// There may be technical or editorial omissions in the programs and their specifications.
// These programs are provided solely dor user appliucation development and user assumes all responsibility for their use.
// Programsand their content are subject to change without notice
// 
// Version	Date		Who			What
// *****************************************************************
// V 1.000	25.09.20	MRupf Oxni	First version
// *****************************************************************

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\sha64debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"

//Configuration Header Includes
#include "DEFINES.h"
#include "AKD_MT.h"

#pragma comment( lib, "c:\\sha\\sha64dll.lib" )
#pragma comment( lib, "c:\\eth\\sha64ethcore.lib" )
#pragma comment( lib, "c:\\ect\\sha64ecatcore.lib" )

#pragma warning(disable:4996)
#pragma warning(disable:4748)

//************************************
//#define SEQ_DEBUG			1
//************************************

//Declare global elements
PETH_STACK		__pUserStack = NULL;		//Ethernet Core Stack (used outside Realtime Task)
PETH_STACK		__pSystemStack = NULL;		//Ethernet Core Stack (used inside Realtime Task)
PSTATION_INFO	__pUserList = NULL;			//Station List (used outside Realtime Task)
PSTATION_INFO	__pSystemList = NULL;		//Station List (used inside Realtime Task)
USHORT			__StationNum = 0;			//Number of Stations
FP_ECAT_ENTER	__fpEcatEnter = NULL;		//Function pointer to Wrapper EcatEnter
FP_ECAT_EXIT	__fpEcatExit = NULL;		//Function pointer to Wrapper EcatExit
ULONG			__EcatState = 0;			//Initial Wrapper State
ULONG			__UpdateCnt = 0;			//Station Update Counter
ULONG			__LoopCnt = 0;				//Realtime Cycle Counter
ULONG			__ReadyCnt = 0;				//Ready state counter
STATE_OBJECT    __StateObject = { 0 };		//Cyclic state object
bool			__bLogicReady = false;		//Logic Ready Flag

#define SEQ_DEBUG			1

// Some variables to play with
int64_t g_sdoData = 0;
AkdMotionTask g_akd1(360.0, 3.0);		// One AKD Axis with 360.0 User Units per revolution and 3 Arms drive
bool g_akd1JogActive = false;
int32_t g_akd1CmdWord = 0;

//***************************
#ifdef SEQ_DEBUG
SEQ_INIT_ELEMENTS(); //Init sequencing elements
#endif
//***************************


__inline PVOID __GetMapDataByName(char* szName, int Index, bool bInput)
{
	int cnt = 0;

	//Loop through station list
	for (USHORT i = 0; i < __StationNum; i++)
		if (strstr(__pUserList[i].szName, szName))
			if (cnt++ == Index)
			{
				//Get station pointer and return index
				if (bInput) { return __pUserList[i].RxTel.s.data; }
				else { return __pUserList[i].TxTel.s.data; }
			}

	//Station not found
	return NULL;
}


// endless task in real time for DS402 OpMode 1
void static DoLogic_MT()
{
	//ToDo: Payload logic
	g_akd1.realTimeTask((TX_MAP_AKD_v1*)__GetMapDataByName((char*)"AKD", 0, TRUE),    //1. Device
		(RX_MAP_AKD_v1*)__GetMapDataByName((char*)"AKD", 0, false));   //1. Device

	//****************************

	//****************************
}

// Real time task
void static AppTask(PVOID)
{
	//Call enter wrapper function
	__EcatState = __fpEcatEnter(
		__pSystemStack,
		__pSystemList,
		(USHORT)__StationNum,
		&__StateObject);


	//Check operation state and increase ready count
	if (__EcatState == ECAT_STATE_READY) { __ReadyCnt--; }
	else { __ReadyCnt = SYNC_CYCLES; }

	//Check ready count
	if (__ReadyCnt == 1)
	{
		SYSTEM_SEQ_CAPTURE("SEQ", __LINE__, (ULONG64) * ((PULONG64)(&__pSystemList[0].TxTel.bytes[0])), false);
		SYSTEM_SEQ_CAPTURE("SEQ", __LINE__, (ULONG64) * ((PULONG64)(&__pSystemList[0].TxTel.bytes[8])), false);

		//**********************************
		if (__bLogicReady) {
			DoLogic_MT();
		}
		//**********************************

		//Update counter
		__UpdateCnt++;
	}

	//Call exit function
	__fpEcatExit();

	//Increase loop count
	__LoopCnt++;
}

void doRun_MT(void) {

	// *** Execute commands form HMI ***
	// Bit 0 = enable
	if (g_akd1CmdWord & 1) {
		g_akd1.enableDrive(!g_akd1.getEnableStatus());
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFFE;
	}
	// Bit 1 = stop (toggle)
	if (g_akd1CmdWord & 2) {
		g_akd1.setStop(!g_akd1.getDriveStop());
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFFD;
	}
	// Bit 2 = reset fault
	if (g_akd1CmdWord & 4) {
		g_akd1.resetFault();
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFFB;
	}
	// Bit 4 = Home move
	if (g_akd1CmdWord & 16) {
		g_akd1.homeAxis();
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFEF;
	}
	// Bit 5 = move abs
	if (g_akd1CmdWord & 32) {
		g_akd1.moveAbsUU(45.0, 600.0, 3600.0, 7200.0);
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFDF;
	}
	// Bit 6 = move rel
	if (g_akd1CmdWord & 64) {
		g_akd1.moveRelUU(90.0, 600.0, 3600.0, 7200.0);
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFFBF;
	}
	// Bit 7 = write sdo
	if (g_akd1CmdWord & 128) {
		g_sdoData = 1000;
		g_akd1.sendSDOCmd(&__pUserList[0], true, 0x356e, 0, 4, (PUCHAR)&g_sdoData);
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFF7F;
	}
	// Bit 8 = read sdo
	if (g_akd1CmdWord & 256) {
		g_sdoData = g_akd1.sendSDOCmd(&__pUserList[0], false, 0x356e, 0, 4, (PUCHAR)&g_sdoData);
		// visuServer.sendInt(14, _sdoData);  // TODO needs to be activated as sonn as the server is ok
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFEFF;
	}
	// Bit 9 = toggel digital output 1
	if (g_akd1CmdWord & 512) {
		g_akd1.setDigOut(1, !(g_akd1.getDigInStat() & 0x10000));
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFDFF;
	}
	// Bit 10 = toggel digital output 2
	if (g_akd1CmdWord & 1024) {
		g_akd1.setDigOut(2, !(g_akd1.getDigInStat() & 0x20000));
		// Reset bit command
		g_akd1CmdWord = g_akd1CmdWord & 0xFBFF;
	}
	// Bit 11 = jog
	if (g_akd1CmdWord & 2048) {
		g_akd1.moveVel(60.0, 3600.0, 3600.0);
		g_akd1JogActive = true;
		// Reset bit command 
		// g_akd1CmdWord = g_akd1CmdWord & 0xF7FF;  Jog Bit is reset by the client
	}
	else if (g_akd1JogActive) {
		g_akd1.moveVel(0.0, 3600.0, 3600.0);
		g_akd1JogActive = false;
	}

	//Display TX and RX information
	//printf("Update Count: %i\r", __UpdateCnt);
	if (g_akd1.getDriveFault()) {
		printf("Drive Fault\r");
	}
	else {
		printf("Position UU: %f\r", g_akd1.getActPosUU());
	}

}


__inline ULONG __SdoControl(void)
{
	//Loop through station list
	for (USHORT i = 0; i < __StationNum; i++)
	{
		PSTATION_INFO pStation = (PSTATION_INFO)&__pUserList[i];

		//Do Device SDO control
		if (strstr(pStation->szName, "AKD")) {
			if (g_akd1.sdoControl_AKD(pStation) != -1) {
				return (-1);
			}
		}
	}

	//Do default PDO assignment
	//return Ecat64PdoAssignment();
	return 0;
}


int main(void)
{
	//******************
#ifdef SEQ_DEBUG
	SEQ_ATTACH();						//Attach to sequence memory
	SEQ_RESET(TRUE, false, NULL, 0);	//Reset/Init sequence memory
#endif
//******************

	printf("\n*** EtherCAT Code Type: [Expert DC] ***\n");

	//Set ethernet mode
	__EcatSetEthernetMode(0, REALTIME_PERIOD, SYNC_CYCLES, TRUE);

	//Required ECAT parameters
	ECAT_PARAMS EcatParams;
	memset(&EcatParams, 0, sizeof(ECAT_PARAMS));
	EcatParams.PhysAddr = DEFAULT_PHYSICAL_ADDRESS;
	EcatParams.LogicalAddr = DEFAULT_LOGICAL_ADDRESS;
	EcatParams.SyncCycles = SYNC_CYCLES;			//Set cycles for synchronisation interval
	EcatParams.EthParams.dev_num = 0;				//Set NIC index [0..7]
	EcatParams.EthParams.period = REALTIME_PERIOD;	//Set realtime period [�sec]
	EcatParams.EthParams.fpAppTask = AppTask;

	//******************************
	//Create ECAT realtime core
	//******************************
	if (ERROR_SUCCESS == Sha64EcatCreate(&EcatParams))
	{
		Sleep(1000);

		//Init global elements
		__pUserStack = EcatParams.EthParams.pUserStack;
		__pSystemStack = EcatParams.EthParams.pSystemStack;
		__pUserList = EcatParams.pUserList;
		__pSystemList = EcatParams.pSystemList;
		__StationNum = EcatParams.StationNum;
		__fpEcatEnter = EcatParams.fpEcatEnter;
		__fpEcatExit = EcatParams.fpEcatExit;

		//Display version information
		Sha64EcatGetVersion(&EcatParams);
		printf("ECTCORE-DLL : %d\n", EcatParams.core_dll_ver);
		printf("ECTCORE-DRV : %d\n", EcatParams.core_drv_ver);
		printf("ETHCORE-DLL : %d\n", EcatParams.EthParams.core_dll_ver);
		printf("ETHCORE-DRV : %d\n", EcatParams.EthParams.core_drv_ver);
		printf("SHA-LIB     : %d\n", EcatParams.EthParams.sha_lib_ver);
		printf("SHA-DRV     : %d\n", EcatParams.EthParams.sha_drv_ver);

		//Display station information
		for (int i = 0; i < __StationNum; i++) {
			printf("Station: %d\n", (ULONG)i);
			if (strstr(__pUserList[i].szName, "AKD")) {
				printf("AKD %d\n", g_akd1.SW_VERSION);
			}
			else {
				printf(__pUserList[i].szName);
			}
		}

		//******************************
		//Enable Stations
		//******************************

		//Change state to INIT
		if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_INIT))
		{
			UCHAR Data[0x100] = { 0 };

			//Reset devices
			Ecat64ResetDevices();
			Ecat64SendCommand(BWR_CMD, 0x0000, 0x300, 8, Data);

			//Set fixed station addresses and
			//Init FMMUs and SYNCMANs
			if (ERROR_SUCCESS == Ecat64InitStationAddresses(EcatParams.PhysAddr))
				if (ERROR_SUCCESS == Ecat64InitFmmus(EcatParams.LogicalAddr))
					if (ERROR_SUCCESS == Ecat64InitSyncManagers())
					{
						//Change state to PRE OPERATIONAL
						//Init PDO assignment
						if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_PRE_OP))
							if (ERROR_SUCCESS == __SdoControl()) {
								//Drift compensation delay [msec]
								ULONG CompLoops = 1000;

								//Init DC immediately after cyclic operation has started
								//and get static master drift per msec (nsec unit)
								if (ERROR_SUCCESS == Ecat64ReadDcLocalTime())
									if (ERROR_SUCCESS == Ecat64CompDcOffset())
										if (ERROR_SUCCESS == Ecat64CompDcPropDelay())
											if (ERROR_SUCCESS == Ecat64CompDcDrift(&CompLoops))
												if (ERROR_SUCCESS == Ecat64DcControl()) {
													//Init process telegrams (required for AL_STATE_SAFE_OP)
													Ecat64InitProcessTelegrams();

													//********************************************************
													//Start cyclic operation
													//********************************************************
													if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_SAFE_OP)) {
														Sleep(500);
														if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_OP)) {

															Sleep(100);

															//Set Logic Ready Flag
															__bLogicReady = TRUE;

															//Do a check loop
															printf("\nPress any key stop EtherCat ...\n");
															while (!kbhit()) {
																doRun_MT();
																//Do some delay
																Sleep(100);
															}

															//Reset Logic Ready Flag
															__bLogicReady = false;
															Sleep(100);

															//********************************************************
															//Stop cyclic operation
															//********************************************************
															Ecat64CyclicStateControl(&__StateObject, AL_STATE_INIT);

														}
													}
												}

							}
					}
		}
		//Destroy ECAT core
		Sha64EcatDestroy(&EcatParams);
	}

	//Check Status
	if (EcatParams.EthParams.pUserStack == NULL) { printf("\n*** Ethernet Stack Failed ***\n"); }
	else if (EcatParams.EthParams.err_cnts.Phy != 0) { printf("\n*** No Link ***\n"); }
	else if (EcatParams.pUserList == NULL) { printf("\n*** No Station List ***\n"); }
	else if (EcatParams.StationNum == 0) { printf("\n*** No Station Found ***\n"); }
	else { printf("\n*** OK ***\n"); }

	//Wait for key press
	printf("\nPress any key ...\n");
	while (!kbhit()) { Sleep(100); }

	//******************
#ifdef SEQ_DEBUG
	SEQ_DETACH(); //Detach from sequence memory
#endif
//******************
}
