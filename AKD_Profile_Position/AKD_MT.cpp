#include "AKD_MT.h"
#include "DEFINES.h"

// Constructor
AkdMotionTask::AkdMotionTask(double posScaling, float driveCurrent)
:txMap(nullptr)
,rxMap(nullptr)
,_posScaling(posScaling)
,_driveIcont(driveCurrent)
{
}

// Return drive fault
bool AkdMotionTask::getDriveFault() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_FAULT_AKD : false;
};

// Return drive warning
bool AkdMotionTask::getDriveWarning() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_WARNING_AKD : false;
};

// Return home status
bool AkdMotionTask::getHomeStatus() {
	return _homeDone;
};

// Return error status
bool AkdMotionTask::getErrorStatus() {
	return _error;
};

// Return enable status
bool AkdMotionTask::getEnableStatus() {
	return _isEnabled;
};

// Return drive stop bit
bool AkdMotionTask::getDriveStop() {
	return _stop;
};

// Return status as bit combined int
uint32_t AkdMotionTask::getDriveStatus() {
	uint32_t temp = (int)_isEnabled * 2;	// Bit 1 Enabled
	temp |= _homeDone * 8; // Bit 3 Home done
	temp |= _error * 32; // Bit 5 Error
	temp |= _stop * 256; // Bit 8 Stop

	// Only read the bus bits if the pointer is valid
	if (txMap != nullptr) {
		temp |= (bool)(txMap->statusWord & 1024) * 4; // Bit 2 In-Pos (Bit 10 in 6041)
		temp |= (bool)(txMap->statusWord & 8) * 16; // Bit 4 Fault (Bit 3 in 6041)
		temp |= (bool)(txMap->statusWord & 128) * 64; // Bit 6 Warning (Bit 7 in 6041)
		temp |= (bool)(txMap->statusWord & 256) * 128; // Bit 7 STO (Bit 8 in 6041)
	}
	return temp;
}

// Get Actual Position as float in User Units
double AkdMotionTask::getActPosUU() {
	return txMap != nullptr ? (double)((_internalPos + (_internalPosOverflow * ULONG_MAX)) / POS_SCALE_DRIVE_AKD) * _posScaling : 0.0;
}

// Get Actual Speed in RPM
double AkdMotionTask::getActVel() {
	return txMap != nullptr ? (double)txMap->actVel : 0.0; // No need for scaling with FW 1-19 ?? / 1000.0;
}

// Get Actual Current in Arms
double AkdMotionTask::getActCurrent() {
	return txMap != nullptr ? ((double)txMap->actCurrent) * _driveIcont / 1000.0f : 0.0;
}

// Get Actual Status of the digital inputs (bit 16 - 22)
uint32_t AkdMotionTask::getDigInStat() {
	uint32_t temp = 0;
	if (txMap != nullptr) {
		temp = (txMap->digitalInput >> 15) & 255;	// get all input (bit 16-22)
		temp |= _digitalOutput;							// Bitwise or the digital output (bit 16-17)
	}
	return temp;
}


//Realtime task this routine has to be called in each cycle
void AkdMotionTask::realTimeTask(const TX_MAP_AKD_v1* Tx_Map, RX_MAP_AKD_v1* Rx_Map) {
	txMap = Tx_Map;
	rxMap = Rx_Map;
	uint16_t cmdWord = 0;
	uint16_t statusWord = 0;

	if (txMap != nullptr) {
		statusWord = (txMap->statusWord & BIT_MASK_DRIVE_STATUS_AKD);
		if (_enable) {
			switch (statusWord)
			{
			// Switch on disabled
			case 0x50:
			case 0x70:
				cmdWord = 0x06;
				break;
			// Ready to switch on
			case 0x31:
				cmdWord = 0x07;
				break;
			// Switched On
			case 0x33:
				cmdWord = 0x0F;
				break;
			// Operation enabled
			case 0x37:
				_isEnabled = true;
				cmdWord = 0x0F;
					
				// Include Stop
				if (_stop) {
					cmdWord = cmdWord | BIT_MASK_DRIVE_STOP_AKD;
					_moveJog = false;
				}
				break;
			}

			// Not Operation enabled
			if (statusWord != 0x37) {
				if (_isEnabled) {
					_enable = false;	// Reset enable in error -> avoid auto enable after a reset!
				}
				_doHome = false;		// Make sure moves can only be called on a already enabled axis
				_moveRel = false;
				_moveAbs = false;
				_moveJog = false;
			}

		} // Else disable
		else {
			_isEnabled = false;
			_moveJog = false;
			cmdWord = 0;
		}

		// Include Reset
		if (_doReset) {
			cmdWord = cmdWord | BIT_MASK_DRIVE_RESET_AKD;
			_doReset = false;
		}
		
		// Include home
		if (_doHome) {
			_homeDone = false;
			_error = false;
			// First switch opmode to Home (6)
			rxMap->opMode = 6;
			if (txMap->opMode == 6) {
				// Check home is started
				if (txMap->statusWord & 0x0010) {
					// Check for done
					if (txMap->statusWord & BIT_MASK_DRIVE_MAN1_AKD) {
						_homeDone = true;
						_internalPosOverflow = 0;
						_doHome = false;
					}
					// Check for error
					if (txMap->statusWord & BIT_MASK_DRIVE_MAN2_AKD) {
						_error = true;
						_doHome = false;
					}
				}
				// Start homing (Bit 4)
				cmdWord = cmdWord | 0x0010;
			}
		}
		else {	// Set profile position mode as default
			if (txMap->opMode != 1) { 
				rxMap->opMode = 1; 
			}
		}

		// Include move rel
		if (_moveRel) {
			cmdWord = cmdWord | 0x0070;	// Bit 4 = Move, Bit 5 = immeditaly, Bit 6 = relative => 0x0070
			_moveRel = false;
		}

		// Include move abs
		if (_moveAbs) {
			cmdWord = cmdWord | 0x0030;	// Bit 4 = Move, Bit 5 = immeditaly, Bit 6 = not relative => 0x0030
			_moveAbs = false;
		}

		// Include jog
		if (_moveJog) {
			// check bit 4 is maximu set every other cycle to generate rising edge
			if (!(txMap->statusWord & BIT_MASK_DRIVE_MAN1_AKD)) {
				cmdWord = cmdWord | 0x0070;
			}
			_counter++;
			// Check for jog-timeout
			if (JOG_TIMEOUT_COUNT > 0) {
				if (_counter > JOG_TIMEOUT_COUNT) {
					_moveJog = false;
				}
			}
		}

		// Read Position and include overflow (upscaling to 64-Bit from 32-Bit)
		if (_internalPos != 0) {
			if (((int64_t)_internalPos - txMap->actPos) > (1073741824)) {
				// + Position overflow
				_internalPosOverflow++;
			}
			else if ((int64_t)(_internalPos - txMap->actPos) < (-1073741824)) {
				// - Position overflow
				_internalPosOverflow--;
			}
		}
		_internalPos = txMap->actPos;

		// Read active mode
		_actMode = txMap->opMode;
		// Map output data
		rxMap->cmdPos = _cmdPos;		// MT.P 
		rxMap->cmdVel = _cmdVel;		// MT.V 
		rxMap->cmdAcc = _cmdAcc;		// MT.ACC 
		rxMap->cmdDec = _cmdDec;		// MT.DEC
		rxMap->digitalOutput = _digitalOutput;
		rxMap->controlWord = cmdWord;	// DS402.CONTROLWORD
	}
}


// Enable/Disable procedure
void AkdMotionTask::enableDrive(bool enable) { 
	_enable = enable && !getDriveFault();
}

// Stop any motion in progress
void AkdMotionTask::setStop(bool stop) {
	_stop = stop;
}

// Reset drive faults
void AkdMotionTask::resetFault() { 
	_doReset = true; 
}

// Home axis
void AkdMotionTask::homeAxis() { 
	_doHome = true; 
}

// Move Relative inputs scaled in UU
void AkdMotionTask::moveRelUU(double moveDistance, double moveVel, double moveAcc, double moveDec) {
	_cmdPos = (int32_t)(moveDistance / _posScaling * POS_SCALE_DRIVE_AKD);	// MT.P [UU]
	_cmdVel = (int32_t)(moveVel / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.V [rpm]
	_cmdAcc = (int32_t)(moveAcc / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.ACC [rpm/s]
	_cmdDec = (int32_t)(moveDec / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.DEC [rpm/s]
	_moveJog = false;
	_moveRel = true;
}
// Move Absolute inputs scaled in UU
void AkdMotionTask::moveAbsUU(double targetPos, double moveVel, double moveAcc, double moveDec) {
	_cmdPos = (int32_t)(targetPos / _posScaling * POS_SCALE_DRIVE_AKD);	// MT.P [UU]
	_cmdVel = (int32_t)(moveVel / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.V [rpm]
	_cmdAcc = (int32_t)(moveAcc / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.ACC [rpm/s]
	_cmdDec = (int32_t)(moveDec / 60.0 * POS_SCALE_DRIVE_AKD);				// MT.DEC [rpm/s]
	_moveJog = false;
	_moveAbs = true;
}

// Jog inputs scaled in UU
void AkdMotionTask::moveVel(double moveVel, double moveAcc, double moveDec) {
	// Get sign for position but not for velocity if move direction is negative
	if (moveVel < 0.0) {
		_cmdPos = (int32_t)(((moveVel / -120.0 * moveVel / moveDec) + moveVel / 600.0) * POS_SCALE_DRIVE_AKD);  // delta P dec + delta P v * 0.1s
		_cmdVel = (int32_t)(moveVel / -60.0 * POS_SCALE_DRIVE_AKD);	// MT.V [rpm]
	}
	else {
		_cmdPos = (int32_t)(((moveVel / 120.0 * moveVel / moveDec) + moveVel / 600.0) * POS_SCALE_DRIVE_AKD);  // delta P dec + delta P v * 0.1s
		_cmdVel = (int32_t)(moveVel / 60.0 * POS_SCALE_DRIVE_AKD);	// MT.V [rpm]
	}
	_cmdAcc = (int32_t)(moveAcc / 60.0 * POS_SCALE_DRIVE_AKD);	// MT.ACC [rpm/s]
	_cmdDec = (int32_t)(moveDec / 60.0 * POS_SCALE_DRIVE_AKD);	// MT.DEC [rpm/s]
	
	// Check if command is contined or not
	if (moveVel == 0.0) {
		_moveJog = false;
	}
	else {
		_counter = 0;
		_moveJog = true;
	}
}

// Set digital output (outIdx 1 or 2)
void AkdMotionTask::setDigOut(int outIdx, bool value) {
	uint32_t temp = (uint32_t)true << (outIdx + 15);
	if (value) {
		_digitalOutput |= temp;	// Set output true
	}
	else {
		_digitalOutput &= ~temp;	// Set output false
	}
}

// Set Move data in ECAT bus units
void AkdMotionTask::moveDataInc(LONG cmdPos, LONG cmdVel, LONG cmdAcc, LONG cmdDec) {
	_cmdPos = cmdPos;	// MT.P 
	_cmdVel = cmdVel;	// MT.V 
	_cmdAcc = cmdAcc;	// MT.ACC 
	_cmdDec = cmdDec;	// MT.DEC
}

// SDO control to the drive
uint64_t AkdMotionTask::sendSDOCmd(PSTATION_INFO pStation, bool write, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data) {
	if (write) {
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -2;
		return 1;
	}
	else {
		ULONG responesLen = 0;
		uint8_t responseData[MAX_ECAT_DATA] = { 0 };
		uint64_t outData2 = 0;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadReq(pStation, idx, subIdx)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadResp(pStation, &responesLen, responseData)) return -2;
		// Byte add the result
		for (size_t i = 0; i < responesLen; i++) {
			outData2 += (uint64_t)responseData[i] << (i * 8);
		}
		// Get the correct sign for 32-Bit variables
		if (responesLen == 4) {
			outData2 = (uint64_t)outData2;
		}
		return outData2;
	}
}

// SDO download request
UINT64 AkdMotionTask::sdoControl_AKD(PSTATION_INFO pStation) {
	uint32_t sdoData = 0;
	UINT64 sdoWriteNo = 0;

	switch (sdoWriteNo)
	{
	case 0:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 1:
		sdoData = 0x00001a00;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x01, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 2:
		sdoData = 0x00001a01;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x02, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 3:
		sdoData = 0x00001a02;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x03, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 4:
		sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 5:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 6:
		sdoData = 0x60410010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 7:
		sdoData = 0x60770010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 8:
		sdoData = 0x60fd0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x03, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 9:
		sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 10:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 11:
		sdoData = 0x60630020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x01, 4, (PUCHAR)&sdoData))break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 12:
		sdoData = 0x606c0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 13:
		sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a01, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 14:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 15:
		sdoData = 0x10260208;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 16:
		sdoData = 0x60610008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 17:
		sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a02, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 18:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 19:
		sdoData = 0x00001600;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x01, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 20:
		sdoData = 0x00001601;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x02, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 21:
		sdoData = 0x00001602;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x03, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 22:
		sdoData = 0x00000003;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 23:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 24:
		sdoData = 0x10260108;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 25:
		sdoData = 0x60400010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 26:
		sdoData = 0x60600008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x03, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 27:
		sdoData = 0x60fe0120;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq (pStation, 0x1600, 0x04, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 28:																																																				
		sdoData = 0x00000004;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 29:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 30:
		sdoData = 0x607a0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 31:
		sdoData = 0x60810020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 32:
		sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1601, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 33:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 34:
		sdoData = 0x60830020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 35:
		sdoData = 0x60840020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 36:
		sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1602, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 37:
		sdoData = 0x00000001;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x6060, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 38:
		sdoData = 0x00000002;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60c2, 0x01, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 39:
		sdoData = 0x000000fd;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60c2, 0x02, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 40:
		sdoData = 0x00030000;		// BitMask for Digital Output (Bit 16+17)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x60fe, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 41:
		sdoData = 0x00000010;		// FBUS.PARAM05 = 16 (To use costume scaling)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x36e9, 0x00, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 42:
		sdoData = (ULONG)POS_SCALE_DRIVE_AKD;//0x00100000;		// DS402.POSGEARSHAFTREV 1048576 (Set costume scaling)
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x6091, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 43:
		//*** Disable SDO automation ***
		pStation->SdoNum = 0;
		return -1;
	}

	//*** Something failed ***
	return sdoWriteNo;
}